### Bidirectional Long Short-Term Memory model to discours parts recognition

1) This a Bi-LSTM model working at character-level with and additional CRF-Layer to automatic annotate parts of the discours in diplomatics latin and french charters. 

### Requeriments
* Tensorflow 1.15.0
* Keras 2.3.1
* CuDA 10.1
* CuDNN 7.0
* joblib==0.14.1


### You can try the paper model architecture using static embeddings in the google colab environment

2) Try the paper Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1Cj8g-dKN7Z-bC1zx0Pb9hNuKPtYIIsA3?usp=sharing)

### You can also try the modeling using the Flair contextual embeddings in colab environment 

3) Try the Flair Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1Fjh2DTTlgQzFWv_XHOeU4NNr6kUjP5BK?usp=sharing)

Additionally, an streamlite demo is available to test this model in combination with NER models to structure charters at this adresse: https://magistermilitum-nlp-tools-streamlit-app-uw6tmq.streamlitapp.com/ (click on medieval charter analyze)

### Training the Model in local

1) In order to generate your model you must activate your Keras engine using Tensorflow as backend. If you want use your GPU you must install CUDA and CuDNNN compatible versions. See more details in [here](https://www.tensorflow.org/install/source_windows). 

2) You must load the corpus and pass the data through the bi-lstm architecture. A pre-loaded architecture can be found in the first colab notebook. During the final step the CRF-layer is added to exploit multidimensional features from the neural hidden-states output. 

3) The original annotated source is a Lombardian corpus contening 4 thousands hand-annotated charters using a pre-TEI format in XML standard. 

4) In order to use your model in production you must load your previous architecture and your word and character dictionnaries. An evaluation environment is also available in the model.py script. 
