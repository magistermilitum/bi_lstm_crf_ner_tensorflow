#!/usr/bin/env python
# coding: utf-8

# In[1]:


from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from functools import reduce
import tensorflow as tf
import numpy as np
import pickle
import pandas as pd


from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import LSTM, Dense, TimeDistributed, Embedding, Bidirectional, Dropout, Conv1D, concatenate, SpatialDropout1D, GlobalMaxPooling1D
from keras.models import Model, Input
from keras_contrib.layers import CRF
from keras.callbacks import ModelCheckpoint

import warnings
warnings.filterwarnings("ignore")

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

from sklearn_crfsuite.metrics import flat_classification_report
from sklearn.metrics import f1_score
from seqeval.metrics import precision_score, recall_score, f1_score, classification_report
from keras.preprocessing.text import text_to_word_sequence

from sklearn.preprocessing import LabelBinarizer
from itertools import chain
import json


# In[2]:


with open("italie_tenor_full.json", "rb") as f:
    sentences=json.load(f)


# In[3]:


sentences=[l for k,l in sentences.items()]


# In[4]:


sentences=[s for s in sentences if len(s)>0]#hay muchas sentencias vacias


# In[5]:


sentences=[[x for x in s if x[2]!="EMPTY"] for s in sentences]


# In[6]:


max_len = 1200
max_len_char = 13


# In[7]:


tags = list(set([x[2] for s in sentences for x in s]))
n_tags = len(tags)+1
words = list(set([x[0] for s in sentences for x in s]))
n_words = len(words); n_words


# In[8]:


word2idx = {w: i + 2 for i, w in enumerate(words)}
word2idx["UNK"] = 1
word2idx["PAD"] = 0
idx2word = {i: w for w, i in word2idx.items()}
tag2idx = {t: i + 1 for i, t in enumerate(tags)}
tag2idx["PAD"] = 0
idx2tag = {i: w for w, i in tag2idx.items()}


# In[ ]:





# In[9]:


from keras.preprocessing.sequence import pad_sequences
X_word = [[word2idx[w[0]] for w in s] for s in sentences]


# In[10]:


X_word = pad_sequences(maxlen=max_len, sequences=X_word, value=word2idx["PAD"], padding='post')


# In[11]:


#averiguamos el numero de diferentes caracteres que existen en nuestros textos
chars = set([w_i for w in words for w_i in w])
n_chars = len(chars)
print(n_chars)


# In[12]:


#generamos y padeamos los textos
char2idx = {c: i + 2 for i, c in enumerate(chars)}
char2idx["UNK"] = 1
char2idx["PAD"] = 0

X_char = []
for sentence in sentences:
    sent_seq = []
    for i in range(max_len):
        word_seq = []
        for j in range(max_len_char):
            try:
                word_seq.append(char2idx.get(sentence[i][0][j]))
            except:
                word_seq.append(char2idx.get("PAD"))
        sent_seq.append(word_seq)
    X_char.append(np.array(sent_seq))


# In[13]:


#generamos las etiquetas de las palabras
y = [[tag2idx[w[2]] for w in s] for s in sentences]


# In[14]:


#padeo del vector de etiquetas
y = pad_sequences(maxlen=max_len, sequences=y, value=tag2idx["PAD"], padding='post')

#num_tag = df['Tag'].nunique()
# One hot encoded labels
y = [to_categorical(i, num_classes = n_tags) for i in y]


# In[15]:


sentences[1]


# In[4]:


import json
with open("word2idx_partes.json", "rb") as f:
    word2idx=json.load(f)
    
with open("char2idx_partes.json", "rb") as f:
    char2idx=json.load(f)
    
with open("tag2idx_partes.json", "rb") as f:
    tag2idx=json.load(f)
    
idx2tag = {i: w for w, i in tag2idx.items()}
idx2word = {i: w for w, i in word2idx.items()}

import json
with open("word2idx_tenor.json", "rb") as f:
    word2idx_b=json.load(f)
    
with open("char2idx_tenor.json", "rb") as f:
    char2idx_b=json.load(f)
    
with open("tag2idx_tenor.json", "rb") as f:
    tag2idx_b=json.load(f)
    
idx2tag_b = {i: w for w, i in tag2idx_b.items()}
idx2word_b = {i: w for w, i in word2idx_b.items()}

max_len=1200
max_len_char=13
max_len_b=1250
max_len_char_b=13


# In[ ]:





# In[5]:


from keras.models import Sequential, load_model
from keras_contrib.losses import crf_loss
from keras_contrib.metrics import crf_viterbi_accuracy


# To load the model
custom_objects={'CRF': CRF,'crf_loss':crf_loss,'crf_viterbi_accuracy':crf_viterbi_accuracy}

# To load a persisted model that uses the CRF layer 
model1 = load_model("BILSTM_CRF_DISPAR_097.h5", custom_objects = custom_objects)
model2 = load_model("BILSTM_CRF_DISTENOR_099.h5", custom_objects = custom_objects)


# In[6]:


# Create a new model instance
model1 = load_model("weights-improvement-30-0.976.hdf5", custom_objects = custom_objects)
# Load the previously saved weights
#model.load_weights("weights-improvement-19-0.971.hdf5")


# In[14]:


MC=pd.read_excel("Montecassino_NER_new.xlsx").values.tolist()


# In[15]:


MC_1={}
count=0
for x in MC:
    if "numero" in str(x[0]):
    #if "cojones" in str(x[0]):
        count+=1
        MC_1[count]=[]
    else:
        MC_1[count].append(x)


# In[16]:


unseen=[MC_1[1]]


# In[18]:


len(unseen)


# In[ ]:


for k,v in MC_1:
    unseenn=[v]
    new_words = [[word2idx[w] if w in word2idx.keys() else word2idx["UNK"] for w in s] for s in unseen]
    new_words = pad_sequences(maxlen=max_len, sequences=new_words, value=word2idx["PAD"], padding='post')
    
    new_chars = []
    for sentence in unseen:
        sent_seq = []
        for i in range(max_len):
            word_seq = []
            for j in range(max_len_char):
                try:
                    if sentence[i][j] in char2idx.keys():
                        word_seq.append(char2idx.get(sentence[i][j]))
                    else:
                         word_seq.append(char2idx.get("UNK"))
                except:
                    word_seq.append(char2idx.get("PAD"))
            sent_seq.append(word_seq)
        new_chars.append(np.array(sent_seq))
        
    result_tag=model1.predict([new_words,np.array(new_chars).reshape((len(new_chars),
                                                                 max_len, max_len_char))], verbose=1)
    p= np.argmax(result_tag, axis=-1)
    
    a=parejas(0)
    texto={}
    for x in a:
        if x[1].split("-")[1] not in texto.keys():
            texto[x[1].split("-")[1]]=[x[0]]
        else:
            texto[x[1].split("-")[1]].append(x[0])
    for k,v in texto.items():
        print(k, "\t : ", " ".join(v), "\n\n")
        
    par=list(zip(unseen[0], [idx2tag[i] for i in p[0]]))
    
    print(par)


# In[164]:


#palabras y tags
new_words = [[word2idx[w] if w in word2idx.keys() else word2idx["UNK"] for w in s] for s in unseen]
#new_tags = [[tag2idx[w[2]] for w in s] for s in unseen]

new_words = pad_sequences(maxlen=max_len, sequences=new_words, value=word2idx["PAD"], padding='post')


# In[165]:


new_chars = []
for sentence in unseen:
    sent_seq = []
    for i in range(max_len):
        word_seq = []
        for j in range(max_len_char):
            try:
                if sentence[i][j] in char2idx.keys():
                    word_seq.append(char2idx.get(sentence[i][j]))
                else:
                     word_seq.append(char2idx.get("UNK"))
            except:
                word_seq.append(char2idx.get("PAD"))
        sent_seq.append(word_seq)
    new_chars.append(np.array(sent_seq))


# In[166]:


result_tag=model1.predict([new_words,np.array(new_chars).reshape((len(new_chars),
                                                                 max_len, max_len_char))], verbose=1)
p= np.argmax(result_tag, axis=-1)
#pred=[i for i in p[0]]


# In[167]:


def parejas(n):
    par=list(zip(unseen[n], [idx2tag[i] for i in p[n]]))
    return (par)


# In[168]:


parejas(0)


# In[169]:


a=parejas(0)
texto={}
for x in a:
    if x[1].split("-")[1] not in texto.keys():
        texto[x[1].split("-")[1]]=[x[0]]
    else:
        texto[x[1].split("-")[1]].append(x[0])
for k,v in texto.items():
    print(k, "\t : ", " ".join(v), "\n\n")
    


# In[19]:


MC_2={}
count=0
for k,v in MC_1.items():
    count+=1
    unseen=[[x[0] for x in v]]
    new_words = [[word2idx[w] if w in word2idx.keys() else word2idx["UNK"] for w in s] for s in unseen]
    new_words = pad_sequences(maxlen=max_len, sequences=new_words, value=word2idx["PAD"], padding='post')

    new_chars = []
    for sentence in unseen:
        sent_seq = []
        for i in range(max_len):
            word_seq = []
            for j in range(max_len_char):
                try:
                    if sentence[i][j] in char2idx.keys():
                        word_seq.append(char2idx.get(sentence[i][j]))
                    else:
                         word_seq.append(char2idx.get("UNK"))
                except:
                    word_seq.append(char2idx.get("PAD"))
            sent_seq.append(word_seq)
        new_chars.append(np.array(sent_seq))

    result_tag=model1.predict([new_words,np.array(new_chars).reshape((len(new_chars),
                                                                 max_len, max_len_char))], verbose=1)
    p= np.argmax(result_tag, axis=-1)
    
    
    
    
    new_words_b = [[word2idx_b[w] if w in word2idx_b.keys() else word2idx_b["UNK"] for w in s] for s in unseen]
    new_words_b = pad_sequences(maxlen=max_len_b, sequences=new_words_b, value=word2idx_b["PAD"], padding='post')

    new_chars_b = []
    for sentence in unseen:
        sent_seq = []
        for i in range(max_len_b):
            word_seq = []
            for j in range(max_len_char_b):
                try:
                    if sentence[i][j] in char2idx_b.keys():
                        word_seq.append(char2idx_b.get(sentence[i][j]))
                    else:
                         word_seq.append(char2idx_b.get("UNK"))
                except:
                    word_seq.append(char2idx_b.get("PAD"))
            sent_seq.append(word_seq)
        new_chars_b.append(np.array(sent_seq))

    result_tag_b=model2.predict([new_words_b,np.array(new_chars_b).reshape((len(new_chars_b),
                                                                 max_len_b, max_len_char_b))], verbose=1)
    q= np.argmax(result_tag_b, axis=-1)
    

    a=list(map(list, zip(v,[idx2tag[i] for i in p[0]], [idx2tag_b[i] for i in q[0]])))
    a=[x[0]+[x[1]]+[x[2]] for x in a]
    MC_2[k]=a


    print(a)
    print("*"*50,k,"*"*50, "\n\n")


# In[228]:


for acte, valeur in MC_2.items():
    for index, word in enumerate(valeur):
        if word[2]=="episcopus" and "PERS" in valeur[index-1][3]:
            print("acte : ", acte, "\t"," ".join([str(x[0]) for x in valeur[index-1:index+5]]), valeur[index-1][5], "\n")


# In[231]:


texto={}
for x in MC_2[419]:
    if x[5].split("-")[1] not in texto.keys():
        texto[x[5].split("-")[1]]=[x[0]]
    else:
        texto[x[5].split("-")[1]].append(x[0])
for k,v in texto.items():
    print(k, "\t : ", " ".join(v), "\n\n")


# In[20]:


with open("MONTECASINO_ALL_new.json", "w") as fp:
    json.dump(MC_2,fp, indent=3)


# In[16]:


data = pd.read_csv('C:/Users/reges/ML/app_montecassino/winemag-data-130k-v2.csv')


# In[17]:


data.head()

