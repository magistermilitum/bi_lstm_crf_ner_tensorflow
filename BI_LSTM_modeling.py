#!/usr/bin/env python
# coding: utf-8

# In[1]:


from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from functools import reduce
import tensorflow as tf
import numpy as np
import pickle
import pandas as pd


from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import LSTM, Dense, TimeDistributed, Embedding, Bidirectional, Dropout, Conv1D, concatenate, SpatialDropout1D, GlobalMaxPooling1D
from keras.models import Model, Input
from keras_contrib.layers import CRF
from keras.callbacks import ModelCheckpoint

import warnings
warnings.filterwarnings("ignore")

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

from sklearn_crfsuite.metrics import flat_classification_report
from sklearn.metrics import f1_score
from seqeval.metrics import precision_score, recall_score, f1_score, classification_report
from keras.preprocessing.text import text_to_word_sequence

from sklearn.preprocessing import LabelBinarizer
from itertools import chain
import json


# In[2]:


if tf.test.gpu_device_name():
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
else:
    print("Please install GPU version of TF")


# In[3]:


with open("italie_tenor_full.json", "rb") as f:
    sentences=json.load(f)


# In[4]:


sentences=[l for k,l in sentences.items()]


# In[5]:


sentences=[s for s in sentences if len(s)>0]#hay muchas sentencias vacias

for index,s in enumerate(sentences):
    caja=[]
    for i,item in enumerate(s):
        if item[2]=="EMPTY":
            caja.append(item[0])
            for ii,itemi in enumerate(s[i:]):
                if itemi[2]=="EMPTY":
                    caja.append(itemi[0])
                else:
                    break
    print(index," ".join(caja), "\n\n")
# In[ ]:





# In[6]:


sentences=[[x for x in s if x[2]!="EMPTY"] for s in sentences]


# In[6]:


a = list(len(s) for s in sentences)
print(np.percentile(a,97))
print(np.percentile(a,85))
print(np.percentile(a,75))
print(np.percentile(a,65))
print(np.percentile(a,55))


# In[7]:


#a partir de aqui ya es especifico
max_len = 1250
max_len_char = 13


# In[8]:


#max([len(x) for x in words])


# In[9]:


sentences[1]


# In[10]:


tags = list(set([x[1] for s in sentences for x in s]))
n_tags = len(tags)+1
words = list(set([x[0] for s in sentences for x in s]))
n_words = len(words); n_words


# In[11]:


n_tags


# In[12]:


word2idx = {w: i + 2 for i, w in enumerate(words)}
word2idx["UNK"] = 1
word2idx["PAD"] = 0
idx2word = {i: w for w, i in word2idx.items()}
tag2idx = {t: i + 1 for i, t in enumerate(tags)}
tag2idx["PAD"] = 0
idx2tag = {i: w for w, i in tag2idx.items()}


# In[13]:


idx2tag.keys()


# In[14]:


from keras.preprocessing.sequence import pad_sequences
X_word = [[word2idx[w[0]] for w in s] for s in sentences]


# In[15]:


X_word = pad_sequences(maxlen=max_len, sequences=X_word, value=word2idx["PAD"], padding='post')


# In[16]:


#averiguamos el numero de diferentes caracteres que existen en nuestros textos
chars = set([w_i for w in words for w_i in w])
n_chars = len(chars)
print(n_chars)


# In[17]:


#generamos y padeamos los textos
char2idx = {c: i + 2 for i, c in enumerate(chars)}
char2idx["UNK"] = 1
char2idx["PAD"] = 0

X_char = []
for sentence in sentences:
    sent_seq = []
    for i in range(max_len):
        word_seq = []
        for j in range(max_len_char):
            try:
                word_seq.append(char2idx.get(sentence[i][0][j]))
            except:
                word_seq.append(char2idx.get("PAD"))
        sent_seq.append(word_seq)
    X_char.append(np.array(sent_seq))


# In[18]:


#generamos las etiquetas de las palabras
y = [[tag2idx[w[1]] for w in s] for s in sentences]


# In[19]:


#padeo del vector de etiquetas
y = pad_sequences(maxlen=max_len, sequences=y, value=tag2idx["PAD"], padding='post')

#num_tag = df['Tag'].nunique()
# One hot encoded labels
y = [to_categorical(i, num_classes = n_tags) for i in y]


# In[20]:


from sklearn.model_selection import train_test_split


# In[21]:


#doble division,una para las palabras, otra para los caracteres
X_word_tr, X_word_te, y_tr, y_te = train_test_split(X_word, y, test_size=0.15, random_state=2018)
X_char_tr, X_char_te, _, _ = train_test_split(X_char, y, test_size=0.15, random_state=2018)


# In[22]:


#save the dictionaries
import json

with open('word2idx_tenor.json', 'w') as fp:
    json.dump(word2idx,fp)
    
with open('char2idx_tenor.json', 'w') as fp:
    json.dump(char2idx,fp)
    
with open('tag2idx_tenor.json', 'w') as fp:
    json.dump(tag2idx,fp)


# In[ ]:





# In[23]:


# Let's check the first sentence before and after processing.
print('*****Before Processing first sentence : *****\n', ' '.join([w[0] for w in sentences[4]]))
print('*****After Processing first sentence : *****\n ', X_word[4])


# In[24]:


character_LSTM_unit = 32
char_embedding_dim = 32
main_lstm_unit = 256 ## Bidirectional 256 + 256 = 512
lstm_recurrent_dropout = 0.5

train_batch_size = 128
train_epochs = 150


# Word Input
word_in = Input(shape=(max_len,), name='word_input_')

# Word Embedding Using Thai2Fit
word_embeddings = Embedding(input_dim=n_words + 2,
                            output_dim=400,input_length=max_len,
                            mask_zero=False,
                            name='word_embedding', trainable=False)(word_in)

# Character Input
char_in = Input(shape=(max_len, max_len_char,), name='char_input')

# Character Embedding
emb_char = TimeDistributed(Embedding(input_dim=n_chars+2, output_dim=char_embedding_dim, 
                           input_length=max_len_char, mask_zero=False))(char_in)

# Character Sequence to Vector via BiLSTM
char_enc = TimeDistributed(Bidirectional(LSTM(units=character_LSTM_unit, return_sequences=False, recurrent_dropout=lstm_recurrent_dropout)))(emb_char)


# Concatenate All Embedding
all_word_embeddings = concatenate([word_embeddings, char_enc])
all_word_embeddings = SpatialDropout1D(0.3)(all_word_embeddings)

# Main Model BiLSTM
main_lstm = Bidirectional(LSTM(units=main_lstm_unit, return_sequences=True,
                               recurrent_dropout=lstm_recurrent_dropout))(all_word_embeddings)
main_lstm = TimeDistributed(Dense(50, activation="relu"))(main_lstm)

# CRF
crf = CRF(n_tags)  # CRF layer
out = crf(main_lstm)  # output

# Model
model = Model([word_in, char_in], out)

model.compile(optimizer="rmsprop", loss=crf.loss_function, metrics=[crf.accuracy])
#optimizer="rmsprop"
model.summary()


# In[25]:


from keras.callbacks import ModelCheckpoint, EarlyStopping
filepath="weights-improvement-{epoch:02d}-{val_crf_viterbi_accuracy:.3f}.hdf5"
es = EarlyStopping(monitor='val_crf_viterbi_accuracy', mode='min', verbose=1, patience=32)
mc = ModelCheckpoint(filepath, monitor='val_crf_viterbi_accuracy', mode='max', verbose=1, save_best_only=True)
# fit model

#filepath="weights-improvement-{epoch:02d}-{val_crf_viterbi_accuracy:.3f}.hdf5"
#checkpoint = ModelCheckpoint(filepath, monitor='val_crf_viterbi_accuracy', verbose=1, save_best_only=True, mode='max')
#callbacks_list = [checkpoint]

with tf.device('/gpu:0'):
    history = model.fit([X_word_tr,
                         np.array(X_char_tr).reshape((len(X_char_tr), max_len, max_len_char))
                         ],
                         np.array(y_tr),
                         batch_size=16, epochs=50, verbose=1,callbacks=[es, mc],
                         validation_data=(
                         [X_word_te,
                         np.array(X_char_te).reshape((len(X_char_te), max_len, max_len_char))
                         ],
                         np.array(y_te))
                       )


# In[26]:


# Evaluation
y_pred = model.predict([X_word_te,np.array(X_char_te).reshape((len(X_char_te),max_len, max_len_char))], verbose=1)
y_pred = np.argmax(y_pred, axis=-1)

y_test_true = np.argmax(y_te, -1)


# In[27]:


y_pred = [[idx2tag[i] for i in row] for row in y_pred]
y_test_true = [[idx2tag[i] for i in row] for row in y_test_true]


# In[28]:


print("F1-score is : {:.1%}".format(f1_score(y_test_true, y_pred)))


# In[46]:


report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[25]:


#mejor resultado con batch 16 al nivel del caracter, mejor crf_viterbi_accuracy 0.98 en la epoca 23. F1-score is : 95.0%
report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[103]:


#mejor resultado con batch 16 al nivel del caracter, mejor crf_viterbi_accuracy 0.97 en la epoca 23. F1-score is : 67.3%
report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[171]:


#mejor resultado con batch 16 al nivel del caracter, mejor crf_viterbi_accuracy 0.97 en la epoca 28. F1-score is : 91.4%?, sin empty
report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[30]:


report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[29]:


report = flat_classification_report(y_pred=y_pred, y_true=y_test_true)
print(report)


# In[ ]:


#cargar los pesos del modelo, luego de haber cargado la arquitectura
#load_filepath="weights-improvement-23-0.997.hdf5"
#model.load_weights(load_filepath)


# In[ ]:





# In[33]:


MC=pd.read_csv("Montecassino_BIO.csv", sep=";").values.tolist()
#MC=pd.read_csv("export_BIO_Saint_Denis.csv", sep=";").values.tolist()


# In[34]:


MC_1={}
count=0
for x in MC:
    if "numero" in str(x[0]):
    #if "cojones" in str(x[0]):
        count+=1
        MC_1[count]=[]
    else:
        MC_1[count].append(x[0])


# In[59]:


unseen=[MC_1[330]]


# In[60]:


#palabras y tags
new_words = [[word2idx[w] if w in word2idx.keys() else word2idx["UNK"] for w in s] for s in unseen]
#new_tags = [[tag2idx[w[2]] for w in s] for s in unseen]

new_words = pad_sequences(maxlen=max_len, sequences=new_words, value=word2idx["PAD"], padding='post')


# In[61]:


#characters
new_chars = []
for sentence in unseen:
    sent_seq = []
    for i in range(max_len):
        word_seq = []
        for j in range(max_len_char):
            try:
                if sentence[i][j] in char2idx.keys():
                    word_seq.append(char2idx.get(sentence[i][j]))
                else:
                     word_seq.append(char2idx.get("UNK"))
            except:
                word_seq.append(char2idx.get("PAD"))
        sent_seq.append(word_seq)
    new_chars.append(np.array(sent_seq))


# In[62]:


result_tag=model.predict([new_words,np.array(new_chars).reshape((len(new_chars),
                                                                 max_len, max_len_char))], verbose=1)
p= np.argmax(result_tag, axis=-1)
#pred=[i for i in p[0]]


# In[63]:


def parejas(n):
    par=list(zip(unseen[n], [idx2tag[i] for i in p[n]]))
    return (par)


# In[64]:


parejas(0)


# In[65]:


parejas(0)


# In[66]:


a=parejas(0)
texto={}
for x in a:
    if x[1].split("-")[1] not in texto.keys():
        texto[x[1].split("-")[1]]=[x[0]]
    else:
        texto[x[1].split("-")[1]].append(x[0])
for k,v in texto.items():
    print(k, "\t : ", " ".join(v), "\n\n")


# In[67]:


a={x[1].append(x[0]) for x in a }


# In[ ]:


a


# In[31]:


model.save("BILSTM_CRF_DISTENOR_099.h5")


# In[32]:


# serialize model to JSON
model_json = model.to_json()
with open("BILSTM_CRF_DISTENOR_099_arch.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
#model.save_weights("model.h5")
#print("Saved model to disk")


# In[ ]:





# In[215]:


[[word2idx[w] if w in word2idx.keys() else word2idx["UNK"] for w in s] for s in unseen]


# In[217]:


id2xword[21170]


# In[29]:


tag2idx


# In[42]:


hist = pd.DataFrame(history.history)

plt.style.use("ggplot")
plt.figure(figsize=(12,12))
plt.plot(hist["crf_viterbi_accuracy"])
plt.plot(hist["val_crf_viterbi_accuracy"])
plt.show()


# In[30]:


hist = pd.DataFrame(history.history)

plt.style.use("ggplot")
plt.figure(figsize=(12,12))
plt.plot(hist["crf_viterbi_accuracy"])
plt.plot(hist["val_crf_viterbi_accuracy"])
plt.show()


# In[ ]:




